module.exports = {
	srcDir: 'src/',
	mode: 'spa',
	head: {
		titleTemplate: '프로젝트 명',
		meta: [
			{ charset: 'UTF-8' },
			{ httpEquiv: 'X-UA-Compatible', content: 'ie=edge' },
			{ httpEquiv: 'Expire', content: '-1' },
			{ httpEquiv: 'Cache-Control', content: 'no-cache' },
			{ httpEquiv: 'pragma', content: 'no-cache' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no' },
			{ name: 'version', content: '2.0.0' },
			{ hid: 'description', name: 'description', content: 'Meta description' }
		],
		htmlAttr: [
			{ lang: 'ko' }
		],
		link: [
			{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
		]
	},
	generate: {
		fallback: true
	},
	css: [
		'~/assets/css/style.css',
		'~/assets/scss/style.scss'
	]
}
