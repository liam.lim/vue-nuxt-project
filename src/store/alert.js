export const state = () => ({
	title: null,
	contents: null,
	callBack: null
});

export const getters = {
	title(state) {
		return state.title;
	},
	contents(state) {
		return state.contents;
	},
	callBack(state) {
		return state.callBack;
	}
}

export const mutations = {
	setAlert(state, { title, contents, callBack }) {
		state.title = title;
		state.contents = contents;
		state.callBack = callBack;
	}
}