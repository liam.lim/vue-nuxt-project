export const state = () => ({
	userInfo: {
		id: '',
		name: '',
		authToken: ''
	},
	loading: false
});

export const getters = {
	userInfo(state) {
		return state.userInfo;
	},
	loading(state) {
		return state.loading;
	}
}

export const mutations = {
	insertUserInfo(state, {id, name, authToken, remember}) {
		state.userInfo.id = id;
		state.userInfo.name = name;
		state.userInfo.authToken = authToken;
		state.userInfo.remember = remember;
	},
	insertLoading(state, isShow) {
		state.loading = isShow;
	}
}
