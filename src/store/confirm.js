export const state = () => ({
	contents: null,
	options: {
		cancelLabel: null,
		confirmLabel: null,
	},
	callBack: null,
	cancel: null
});

export const getters = {
	contents(state) {
		return state.contents;
	},
	options(state) {
		return state.options;
	},
	callBack(state) {
		return state.callBack;
	},
	cancel(state) {
		return state.cancel;
	}
}

export const mutations = {
	setConfirm(state, { contents, options, callBack, cancel }) {
		state.contents = contents;
		state.options = Object.assign(state.options, options);
		state.callBack = callBack;
		state.cancel = cancel;
	}
}