import axios from 'axios';
import env from './env';
// import GibberishAES from 'gibberish-aes';
// import { MockDataServer } from './fakeApi';
// MockDataServer();

const msaId = 'msatest';
const msaPwd = 'msaauthtest';
// Basic bXNhdGVzdDptc2FhdXRodGVzdA==

const API = (() => {
	const defaultConfig = {
		withCredentials: false,
		headers: {
			'Content-type': 'application/json',
			'Access-Control-Allow-Origin': '*',
			'Access-Control-Allow-Methods': '*',
			'Access-Control-Allow-Headers': true,
			'Encoding': 'UTF-8',
			'Authorization': 'Basic ' + window.btoa(`${msaId}:${msaPwd}`)
		},
	}

	const defaultOptions = {
		withCredentials: false,
        headers: {
			'Content-type': 'application/json',
			'Access-Control-Allow-Origin': '*',
			'Access-Control-Allow-Methods': '*',
			'Access-Control-Allow-Headers': true,
			'Encoding': 'UTF-8',
			'Authorization': 'Basic ' + window.btoa(`${msaId}:${msaPwd}`)
        },
    };


	// fake api
	// return {
	// 	get:(uri, config) => {
	// 		return axios.get(uri, Object.assign(defaultConfig, config));
	// 	},
	// 	post:(uri, data, config) => {
	// 		return axios.post(uri, data, Object.assign(defaultConfig, config));
	// 	},
	// 	put:(uri, data, config) => {
	// 		return axios.put(uri, data, Object.assign(defaultConfig, config));
	// 	},
	// 	delete:(uri) => {
	// 		return axios.delete(uri, Object.assign(defaultConfig, config));
	// 	}
	// }

	// real api
	return {
		get:(uri, config) => {
			return axios.get(`${ env.baseUri }${ uri }`, Object.assign(defaultConfig, config));
		},
		post:(uri, data, config) => {
			return axios.post(`${ env.baseUri }${ uri }`, data, Object.assign(defaultConfig, config));
		},
		post2:(uri, data) => {
			return axios.post(`${ env.baseUri }${ uri }`, data);
		},
		put:(uri, data, config) => {
			return axios.put(`${ env.baseUri }${ uri }`, data, Object.assign(defaultConfig, config));
		},
		patch:(uri, data, options) => {
			// return axios.patch(`${ env.baseUri }${ uri }`, data, Object.assign(defaultConfig, config));
			return axios.patch(`${ env.baseUri }${ uri }`, data, { ...defaultOptions, ...options })
		},
		delete:(uri) => {
			return axios.delete(`${ env.baseUri }${ uri }`, Object.assign(defaultConfig, config));
		}
	}
})();

export { API };
